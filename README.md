# APAM - Asclepios Privacy and Analytics Module

APAM is a module created for the task 2.6 of the Asclepios project. His role is to provide means for both, Patient and Data Protection Officer (DPO) to analyse and assess the patient data management   and  the organisation behaviour.

## Web Interfaces

APAM provides s REST API interface with multiple endpoints|functions

## Usage
The file requirements.txt contains all the libraries used, mainly they are:
[Django](https://pypi.org/project/Django/)|[Django - Rest Framework](https://pypi.org/project/djangorestframework/) for development and [Faker](https://pypi.org/project/Faker/) | [Names](https://pypi.org/project/names/) to populate the database with fake data and other supporting libraries.

To deploy just run in the terminal the code below
Install MySQL and create database "mydb", password "newpwd123"
Install and start ELK services(Logstash, ElasticSearch and Kibana)
Start Logstash with the configuration file /logstash/pipeline/01-logstash-mysql.conf

```bash
pip3 install -r requirements.txt # Install the requirements
```
```python3
python3 manage.py inspectdb > log_models/models.py
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py shell < log_models/fill_db_script.py
python3 manage.py runserver # Runs the APAM module locally
```

After running, the REST API interface can be accessed at http://127.0.0.1:8000/apam/
After running, the Kibana interface can be accessed at http://127.0.0.1:5601/

Import Kibana dashboards examples: /Kibana/dashboards.ndjson


## Documentation
The full documentation including usage tutorials, list of endpoints and available parameters, filters etc can be found [here](documentation).

## License
TODO