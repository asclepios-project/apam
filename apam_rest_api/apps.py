from django.apps import AppConfig


class ApamRestApiConfig(AppConfig):
    name = 'apam_rest_api'
