from django.urls import path
from . import views

urlpatterns = [
    path('', views.root, name='root'),
    path('patient/full_log', views.get_patient_full_log, name='get_patient_full_log'),
    path('patient/outcome_metrics', views.get_outcome_metrics, name='get_outcome_metrics'),
    path('patient/access_period_metrics', views.get_access_period_metrics, name='get_outcome_metrics'),
    path('patient/action_metrics', views.get_action_metrics, name='get_outcome_metrics'),
    path('organisation/full_log', views.get_org_full_log, name='get_org_full_log'),
    path('organisation/outcome_metrics', views.get_org_outcome_metrics, name='get_org_outcome_metrics'),
    path('organisation/location_metrics', views.get_org_location_metrics, name='get_org_location_metrics'),
    path('organisation/access_period_metrics', views.get_org_access_period_metrics, name='get_org_access_period_metrics'),
    path('organisation/action_metrics', views.get_org_action_metrics, name='get_org_action_metrics'),
    path('organisation/outlier_detection', views.get_org_outliers, name='get_org_outlier'),
]