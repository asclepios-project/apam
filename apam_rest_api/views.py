#############################################
############ Web server imports #############
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from log_model.models import ApamLog, Action, Event
from django.shortcuts import get_object_or_404
from django.core import serializers
from django.http import HttpResponse
import json
from datetime import datetime

#############################################
# Machine learning and data process imports #
import pandas as pd
from django_pandas.io import read_frame   # Import django_pandas.io read frame
# from sklearn.cluster import KMeans
# from sklearn.decomposition import PCA
# from sklearn.preprocessing import StandardScaler
# from sklearn.preprocessing import LabelEncoder
import numpy as np

query_to_parameters = {
    'subjectid' : 'subject_id',
    'organisationid' : 'organisation_id',
    'recorded__date__gte' : 'start_date',
    'recorded__date__lte' : 'end_date',
    'recorded__hour__gte' : 'start_hour',
    'recorded__hour__lte' : 'end_hour',
    'agentcountry__exact' : 'source_country',
    'agentcity__exact' : 'source_city',
    'pca_number' : 'pca_number',
    'k_number' : 'k_number',
    'farthest_percent' : 'top_percent',
    'pca_variance_ratio' : 'pca_variance_ratio'
    
}

def return_params(request):
    query_params = {
        'subjectid' : request.query_params.get("subject_id", None),
        'organisationid' : request.query_params.get("organisation_id", None),
        'recorded__date__gte' : request.query_params.get("start_date", None),
        'recorded__date__lte' : request.query_params.get("end_date", None),
        'recorded__hour__gte' : request.query_params.get("start_hour", None),
        'recorded__hour__lte' : request.query_params.get("end_hour", None),
        'agentcountry__exact' : request.query_params.get("source_country", None),
        'agentcity__exact' : request.query_params.get("source_city", None),
        'agentrole_exact' : request.query_params.get('agent_role', None),
        'actioncode__exact' : request.query_params.get("agent_action", None),
        'organisation__organisationid__organisationname__exact' : request.query_params.get("organisation_name", None),
    }

    other_params = {
        'is_patient' : request.query_params.get("agent_is_patient", None),
        'pca_number' : request.query_params.get("pca_number", None),
        'k_number' : request.query_params.get("k_number", None),
        'farthest_percent' : request.query_params.get("top_percent", None),
    }
    return query_params, other_params

def treat_query(dataframe, qs_log):
    outcomes = []
    countries = []
    organisations = []
    actions = []
    events = []

    for q in qs_log:
        outcomes.append(q.outcome.name)
        countries.append(q.requestorcountry)
        actions.append(q.idaction.name)
        events.append(q.idevent.name)
        if q.organisation is not None:
            organisations.append(q.organisation.organisationname)
        else:
            organisations.append(None)

    dataframe["outcome"] = outcomes
    dataframe["actioncode"] = actions
    dataframe["organisation"] = organisations
    dataframe["eventcode"] = events
    
    return dataframe

##################################################################################################
##################################################################################################
@api_view(['GET'])
def root(request):
    """
    All requests and params available
    """
    if request.method == 'GET':        
        # Responses
        available_urls = {
            "available_urls" : {
                "/patient/full_log" : 'Returns the audit logs related to the patient',
                "/patient/outcome_metrics" : "Returns metrics about the outcomes related to the requests on the patient's EMR",
                "/patient/access_period_metrics" : "Returns metrics about the period of access on the patient's EMR",
                "/patient/action_metrics" : "Returns metrics about the actions done on the patient's EMR",
                "/organisation/full_log" : "Returns the audit logs related to the Organisation",
                "/organisation/outcome_metrics" : "Returns metrics about the outcomes related to the requests performed by the organisation",
                "/organisation/location_metrics" : "Returns metrics about the location related to the requests performed by the organisation",
                "/organisation/access_period_metrics" : "Returns metrics about the period of access on the patient's EMR by the organisation",
                "/organisation/action_metrics" : "Returns metrics about the actions related to the requests performed by the organisation",
                "/organisation/outliers" : "Returns the audit logs considered outliers"
            } }

        return Response(available_urls ,status=status.HTTP_200_OK)

##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################

def fill_events_patient(dataframe, events_list):
    for index, row in dataframe.iterrows():
        if int(row["organisationid"]) == int(filters["subjectid"]):
            event = {
                "time_recorded" : row["recorded"],
                "agent" : "Patient",
                "agent_action" : row["actioncode"],
                "outcome" : row["outcome"],
                "agent_source_country" : row["agentcountry"]
            }
        else:
            event = {
                "time_recorded" : row["recorded"],
                "agent" : "Healthcare Professional",
                "agent_action" : row["actioncode"],
                "outcome" : row["outcome"],
                "agent_organisation" : row["organisation"],
                "agent_source_country" : row["agentcountry"],
                "agent_department" : row["agentdepartment"],
                "agent_role" : row["agentrole"],
            }

        events_list[row["idapam_log"]] = event
    return events_list

@api_view(['GET'])
def get_patient_full_log(request):
    """
    Full log related to the patient
    """
    if request.method == 'GET':        
        # Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are userID (number)",
            "Example" : "url/myData?userID=10000" 
        }

        filters, others = return_params(request)

        #### Body of code
        if filters["subjectid"] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)

            events_list = {}
            event_list = fill_events_patient(df_response, events_list)

            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "events" : events_list

            }
            return Response(response, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_outcome_metrics(request):
    """
    Get metrics about access requests on patient's EMR
    """
    if request.method == 'GET':
        # Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are userID (number)",
            "Example" : "url/myData?userID=10000" }

        filters, others = return_params(request)

        #### Body of code
        if filters["subjectid"] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)

            df_response = treat_query(df_response, qs_log)
            
            ###### Overral
            overral = {}
            df0 = df_response.groupby(["outcome"])["outcome"].count()
            for key in df0.keys():
                overral[key] = {
                    "outcomes" : df0[key],
                    "percentage_from_total" : round(df0[key] / len(df_response) * 100, 2)
                }

            ###### Patient
            df_patient = df_response[df_response["agentid"] == int(filters["subjectid"])]
            patient = {}
            df0 = df_patient.groupby(["outcome"])["outcome"].count()
            for key in df0.keys():
                patient[key] = {
                    "outcomes" : df0[key],
                    "percentage_from_total" : round(df0[key] / len(df_response) * 100, 2)
                }

            ###### Healthcare Professional
            df_hp = df_response[df_response["agentid"] != int(filters["subjectid"])]
            hp = {}
            df0 = df_hp.groupby(["outcome"])["outcome"].count()
            for key in df0.keys():
                hp[key] = {
                    "outcomes" : df0[key],
                    "percent_from_total" : round(df0[key] / len(df_response) * 100, 2),
                    "percent_from_healthcare_as_agent" : round(df0[key] / len(df_hp) * 100, 2)
                }
            
            ###### Organisation
            df0 = df_response.groupby(["organisation"])["outcome"].count()
            orgs = {}
            for key in df0.keys():
                orgs[key] = {
                    "entries" : df0[key],
                    "percent_from_total" : round(df0[key] / len(df_response) * 100, 2),
                    "per_roles" : {},
                }                    
            
            df1 = df_response.groupby(["organisation", "agentrole"])["agentrole"].count()
            for key in df1.keys():
                orgs[key[0]]["per_roles"][key[1]] = {
                    "number_of_entries" : df1[key],
                    "percent_from_organisation_entries" : round(df1[key] / orgs[key[0]]["entries"] * 100, 2),
                    "outcomes" : {}
                }

            df2 = df_response.groupby(["organisation", "agentrole", "outcome"])["outcome"].count()
            for key in df2.keys():
                orgs[key[0]]["per_roles"][key[1]]["outcomes"][key[2]] = {
                    "number_of_entries" : df2[key],
                    "percent_from_organisation_entries" : round(df2[key] / orgs[key[0]]["entries"] * 100, 2),
                }
                        
            events_list = {}
            event_list = fill_events_patient(df_response, events_list)                    

            resp = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "outcome_metrics" : {
                    "total_entries" : len(df_response),
                    "overall" : overral,
                    "patient_as_agent" : patient,
                    "healthcare_professional_as_agent" : hp,
                    "per_organisation" : orgs,
                },

                "events" : events_list,
            }

            return Response(resp, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_access_period_metrics(request):
    """
    Get metrics about the period processing the patient's EMR
    """
    if request.method == 'GET':
        # Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are userID (number)",
            "Example" : "url/myData?userID=10000" 
        }

        filters, others = return_params(request)

        #### Body of code
        if filters["subjectid"] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)

            ##### get access period metrics, min, mean, max by organisation and professional roles
            ### TODO

            events_list = {}
            events_list = fill_events_patient(df_response, events_list)
            
            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "events" : events_list
            }

            return Response(response, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_action_metrics(request):
    """
    Get metrics about the actions done on the patient's EMR
    """
    if request.method == 'GET':
        
        # Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are userID (number)",
            "Example" : "url/myData?userID=10000" }

        filters, others = return_params(request)

        #### Body of code
        if filters["subjectid"] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            response = {}

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)

            ###### Overall
            overall = {}
            df = df_response.groupby(["actioncode"])["actioncode"].count()
            for key in df.keys():
                overral[key] = {
                    "actions" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2)
                }

            ###### Patient
            df_patient = df_response[df_response["agentid"] == int(filters["subjectid"])]
            patient = {}
            df = df_patient.groupby(["actioncode"])["actioncode"].count()
            for key in df.keys():
                patient[key] = {
                    "actions" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2)
                }

            ###### Healthcare Professional
            df_hp = df_response[df_response["agentid"] != int(filters["subjectid"])]
            hp = {}
            df = df_hp.groupby(["actioncode"])["actioncode"].count()
            for key in df.keys():
                hp[key] = {
                    "actions" : df[key],
                    "percent_from_total" : round(df[key] / len(df_response) * 100, 2),
                    "percent_from_healthcare_as_agent" : round(df[key] / len(df_hp) * 100, 2)
                }
            
            ###### Grouping by organizations
            df0 = df_response.groupby(["organisation"])["actioncode"].count()
            orgs = {}
            for key in df0.keys():
                orgs[key] = {
                    "entries" : df0[key],
                    "percent_from_total" : round(df0[key] / len(df_response) * 100, 2),
                    "actions" : {},
                    "per_roles" : {},
                }                    
            
            df1 = df_response.groupby(["organisation", "actioncode"])["actioncode"].count()
            for key in df1.keys():
                orgs[key[0]]["actions"][key[1]] = {
                    "entries" : df1[key],
                    "percent_from_total" : round(df1[key] / len(df_response) * 100, 2),
                    "percent_from_organisation_entries" : round(df1[key] / orgs[key[0]]["entries"] * 100, 2),
                }
            
            df1 = df_response.groupby(["organisation", "agentrole"])["agentrole"].count()
            for key in df1.keys():
                orgs[key[0]]["per_roles"][key[1]] = {
                    "entries" : df1[key],
                    "percent_from_total" : round(df1[key] / len(df_response) * 100, 2),
                    "percent_from_organisation_entries" : round(df1[key] / orgs[key[0]]["entries"] * 100, 2),
                    "actions" : {}
                }
            
            df1 = df_response.groupby(["organisation", "agentrole", "actioncode"])["agentrole"].count()
            for key in df1.keys():
                orgs[key[0]]["per_roles"][key[1]]["actions"][key[2]] = {
                    "entries" : df1[key],
                    "percent_from_total" : round(df1[key] / len(df_response) * 100, 2),
                    "percent_from_organisation_entries" : round(df1[key] / orgs[key[0]]["entries"] * 100, 2),
                    "percent_from_role_entries" : round(df1[key] / orgs[key[0]]["per_roles"][key[1]]["entries"] * 100, 2),

                }

            events_list = {}
            events_list = fill_events_patient(df_response, events_list)
            
            ###### Response
            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "action_metrics" : {
                    "overrall" : overral, 
                    "patient_as_agent" : patient,
                    "healthcare_professional_as_agent" : hp,
                    "actions_per_organisation" : orgs
                },
                "events" : events_list
            }

        return Response(response, status=status.HTTP_200_OK) 
       
##################################################################################################
##################################################################################################
##################################################################################################
##################################################################################################

def fill_events_organisation(dataframe, events_list):
    for index, row in df_response.iterrows():
        event = {
            "time_recorded" : row["recorded"],
            "author" : "Healthcare Professional",
            "action" : row["actioncode"],
            "outcome" : row["outcome"],
            "organisation" : row["organisation"],
            "source_country" : row["agentcountry"],
            "role" : row["agentrolee"]
        }
        events_list[row["idapam_log"]] = event
    
    return events_list

@api_view(['GET'])
def get_org_full_log(request):
    """
    Full log related to the organization
    """
    if request.method == 'GET':        
        #### Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are orgID (number)",
            "Example" : "url/orgFullLog?orgID=10000" }
        
        #### Query params
        filters, others = return_params(request)

        #### Body of code
        if filters['organisationid'] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)


            events_list = {}
            events_list = fill_events_organisation(df_response, events_list)


            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "events" : events_list }

            return Response(response, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_org_outcome_metrics(request):
    """
    Outcome metrics of the requests done by agents related to the organisation
    """
    if request.method == 'GET':        
        #### Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are orgID (number)",
            "Example" : "url/orgFullLog?orgID=10000" }
        
        #### Query params
        filters, others = return_params(request)

        #### Body of code
        if filters['organisationid'] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)

            ###### Overall
            overall = {}
            df = df_response.groupby(["outcome"])["outcome"].count()
            for key in df.keys():
                overall[key] = {
                    "outcomes" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2)
                }
            
            ###### Per Roles
            per_roles = {}
            df = df_response.groupby(["agentrole"])["outcome"].count()
            for key in df.keys():
                per_roles[key] = {
                    "outcomes" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2),
                }
            df = df_response.groupby(["agentrole", "outcome"])["outcome"].count()
            for key in df.keys():
                per_roles[key[0]][key[1]] = {
                    "outcomes" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2),
                    "percentage_from_role_total" : round(df[key] / per_roles[key[0]]["outcomes"] * 100, 2),
                }

            events_list = {}
            events_list = fill_events_organisation(df_response, events_list)


            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "outcome_metrics" : {
                    "total_entries" : len(df_response),
                    "overall" : overall,
                    "per_roles" : per_roles,
                },
                "events" : events_list }

            return Response(response, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_org_access_period_metrics(request):
    """
    Get metrics about the period processing the patient's EMR
    """
    if request.method == 'GET':
        # Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are userID (number)",
            "Example" : "url/myData?userID=10000" 
        }

        filters, others = return_params(request)

        #### Body of code
        if filters["organisationid"] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)

            events_list = {}
            events_list = fill_events_organisation(df_response, events_list)
            
            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "events" : events_list
            }

            return Response(response, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_org_action_metrics(request):
    """
    Action metrics of the requests done by agents related to the organisation
    """
    if request.method == 'GET':        
        #### Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are orgID (number)",
            "Example" : "url/orgFullLog?orgID=10000" }
        
        #### Query params
        filters, others = return_params(request)

        #### Body of code
        if filters['organisationid'] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)

            ###### Overall
            overall = {}
            df = df_response.groupby(["actioncode"])["actioncode"].count()
            for key in df.keys():
                overall[key] = {
                    "actions" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2)
                }
            
            ###### Per Roles
            per_roles = {}
            df = df_response.groupby(["agentrole"])["actioncode"].count()
            for key in df.keys():
                per_roles[key] = {
                    "actions" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2),
                }
            df = df_response.groupby(["agentrole", "actioncode"])["actioncode"].count()
            for key in df.keys():
                per_roles[key[0]][key[1]] = {
                    "actions" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2),
                    "percentage_from_role_total" : round(df[key] / per_roles[key[0]]["actions"] * 100, 2),
                }

            events_list = {}
            events_list = fill_events_organisation(df_response, events_list)


            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "action_metrics" : {
                    "total_entries" : len(df_response),
                    "overall" : overall,
                    "per_roles" : per_roles,
                },
                "events" : events_list }

            return Response(response, status=status.HTTP_200_OK)

@api_view(['GET'])
def get_org_location_metrics(request):
    """
    Location metrics of the requests done by agents related to the organisation
    """
    if request.method == 'GET':        
        #### Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are orgID (number)",
            "Example" : "url/orgFullLog?orgID=10000" }
        
        #### Query params
        filters, others = return_params(request)

        #### Body of code
        if filters['organization'] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }

            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)

            ###### Overall
            overall = {}
            df = df_response.groupby(["agentcountry"])["agentcountry"].count()
            for key in df.keys():
                overall[key] = {
                    "requests" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2)
                }
            
            ###### Per Roles
            per_roles = {}
            df = df_response.groupby(["agentrole"])["agentcountry"].count()
            for key in df.keys():
                per_roles[key] = {
                    "requests" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2),
                }
            df = df_response.groupby(["agentrole", "agentcountry"])["agentcountry"].count()
            for key in df.keys():
                per_roles[key[0]][key[1]] = {
                    "requests" : df[key],
                    "percentage_from_total" : round(df[key] / len(df_response) * 100, 2),
                    "percentage_from_role_total" : round(df[key] / per_roles[key[0]]["requests"] * 100, 2),
                }

            events_list = {}
            events_list = fill_events_organisation(df_response, events_list)


            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              kwargs.items()}    
                },
                "location_metrics" : {
                    "total_entries" : len(df_response),
                    "overall" : overall,
                    "per_roles" : per_roles,
                },
                "events" : events_list }

            return Response(response, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_org_outliers(request):
    """
    Get events classified as outliers after using pca and kmeans
    """
    if request.method == 'GET':
        #### Responses
        invalid_param_response = {
            "Error" : "Invalid parameter found in the URL",
            "Params" : "Acceptable params are userID (number)",
            "Example" : "url/orgOutiers?orgID=10000" }
        response = {}

        #### Query params
        filters, others = return_params(request)

        outlier_percent = request.query_params.get('farthest_percent', 10)
        pca_variance_ratio = request.query_params.get('pca', 0.95)
        pca_number = request.query_params.get('pca', None)
        k_number = request.query_params.get('k', 3)


        #### Body of code
        if filters["organisationid"] == None:
            return Response(invalid_param_response ,status=status.HTTP_400_BAD_REQUEST)
        else:
            #### Remove None values from kwargs
            kwargs = {key:value for key,value in 
              filters.items()
              if value is not None }
            
            others = {key:value for key,value in 
              others.items()
              if value is not None }

            #### query the database into pandas dataframe
            qs_log = ApamLog.objects.filter(**kwargs)
            df_response = read_frame(qs_log)
            df_response = treat_query(df_response, qs_log)
            df = df_response.drop(columns=["idapam_log", "agentattributes", "agentname", "entityobject", "entitydetails"])

            #### PCA and K-means application
            # Normalize
            #df.iloc[:,0:3] = StandardScaler().fit_transform(df.iloc[:,0:3])
            df.iloc[:,0:1] = LabelEncoder().fit_transform(df.iloc[:,0:1])
            df.iloc[:,1:2] = LabelEncoder().fit_transform(df.iloc[:,1:2])
            df.iloc[:,2:3] = LabelEncoder().fit_transform(df.iloc[:,2:3])
            df.iloc[:,3:4] = LabelEncoder().fit_transform(df.iloc[:,3:4])
            df.iloc[:,4:5] = LabelEncoder().fit_transform(df.iloc[:,4:5])
            df.iloc[:,5:6] = LabelEncoder().fit_transform(df.iloc[:,5:6])
            df.iloc[:,6:7] = LabelEncoder().fit_transform(df.iloc[:,6:7])
            df.iloc[:,7:8] = LabelEncoder().fit_transform(df.iloc[:,7:8])
            df.iloc[:,8:9] = LabelEncoder().fit_transform(df.iloc[:,8:9])
            df.iloc[:,9:10] = LabelEncoder().fit_transform(df.iloc[:,9:10])
            df.iloc[:,10:11] = LabelEncoder().fit_transform(df.iloc[:,10:11])
            df.iloc[:,11:12] = LabelEncoder().fit_transform(df.iloc[:,11:12])


            # Create a PCA instance: pca
            if others["pca_number"] is None:
                pca = PCA(n_components=pca_variance_ratio)
            pca = PCA(n_components=int(others["pca_number"]))
            principalComponents = pca.fit_transform(df.iloc[:, 0:12])
            pca_resp = len(principalComponents[0])
            print(pca_resp)

            # Create a KMeans instance with k clusters: model
            model = KMeans(n_clusters=k_number)
            
            # Fit model to samples
            model.fit(principalComponents)

            #### Generate response with top % farthest from each cluster
            labels = model.labels_
            centroids = model.cluster_centers_

            top_percent = {i: np.where(model.labels_ == i)[0] for i in range(model.n_clusters)}
            df_response["cluster_n"] = np.nan

            for key, value in top_percent.items():
                df_response.loc[value[1], "cluster_n"] = key
            qs_json = serializers.serialize('json', qs_log)

            df_response = df_response.dropna(subset=['cluster_n'])
            k_resp = k_number

            events_list = {}
            events_list = fill_events_organisation(df_response, events_list)

            response = {
                "request_details" : {
                    "response" : status.HTTP_200_OK,
                    "datetime" : datetime.now(),
                    "filter_parameters" : {query_to_parameters[key]:value for key,value in 
              {**kwargs, **others}.items()},                    
                },
                "params_used" : {
                    "pca_variance_ratio" : pca_variance_ratio,
                    "pca" : pca_resp,
                    "k" : k_resp,
                    "farthest_percent" : outlier_percent,
                },
                "outliers_events" : event_list 
            }

            return Response(response, status=status.HTTP_200_OK)