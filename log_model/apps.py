from django.apps import AppConfig


class LogModelConfig(AppConfig):
    name = 'log_model'
