from log_model.models import Action, Event, ApamLog, Outcome, Organisation
from tqdm import tqdm
import names, random
from datetime import datetime
from faker import Faker
from faker.providers import internet
from ip2geotools.databases.noncommercial import DbIpCity
fake = Faker('nl_NL')
fake.add_provider(internet)


ApamLog.objects.all().delete()
Outcome.objects.all().delete()
Event.objects.all().delete()
Action.objects.all().delete()
Organisation.objects.all().delete()

#############################
### Actions following FHIR ##
actions = [(10001, "Create", "Create a new object"), (10002, "Read|View|Print", "Display or Print data"), (10003, "Update", "Update data"), (10004, "Delete", "Delete items"),
(10005, "Execute", "Perform a system or application function")] 

print("Filling actions from FHIR standard => https://www.hl7.org/fhir/auditevent.html")
for entry in actions:
    Action.objects.create(actioncode=entry[0],
    actionname=entry[1],
    actiondescription=entry[2])

#############################
### Events following FHIR ###
events = [(110100, "Application Activity", "Application Activity has taken place"), (110101, "Audit Log User", "Audit Log has been used"),
(110102, "Begin Transfer DICOM Instances", "Storage of DICOM Instances has begun"), (110103, "DICOM Instances Accessed", "DICOM Instances have been accessed"),
(110104, "DICOM Instances Transferred", "Storage of DICOM Instances has been completed"), (110105, "DICOM Study Deleted", "Entire study has been deleted"),
(110106, "Export", "Data has been exported out of the system"), (110107, "Import", "Data has been imported into the system"), (110108, "Network Entry", "System has joined or left the network"),
(110109, "Order Record", "Order record has been created, read, updated or deleted"), (110110, "Patient Record", "Patient record has been created, read, updated or deleted"),
(110111, "Procedure Record", "Procedure record has been created, read, updated or deleted"), (110112, "Query", "Query has been made"), (110113, "Security Alert", "Security alert has been raised"),
(110114, "User Authentication", "User authentication has been attempted")]

print("Filling events from FHIR standard => https://www.hl7.org/fhir/auditevent.html")
for entry in events:
    Event.objects.create(eventcode=entry[0],
    eventname=entry[1],
    eventdescription=entry[2])

#############################
### Outcome following FHIR ##
outcomes = [(0, "Success", "The action was done successfully"), (1, "Denied", "The action was denied"), (10, "Failure", "The action failed")]

print("Filling outcomes from FHIR standard AuditEvent => https://www.hl7.org/fhir/auditevent.html")
for entry in outcomes:
    Outcome.objects.create(outcomecode=entry[0],
    outcomename=entry[1],
    outcomedescription=entry[2])


hospitals = [(10000, 'VU medisch centrum','VUMC', 'NL', 'Amsterdam'), (10001, 'Academisch Medisch Centrum', 'AMC', 'NL', 'Amsterdam'), (10002, 'Universitair Medisch Centrum Groningen', 'UMCG', 'NL', 'Groningen'), (10003,'Universitair Medisch Centrum Utrecht', 'UMCU', 'NL', 'Utrecht'),
(10005, 'Maastricht Universitair Medisch Centrum', 'MUMC+', 'NL', 'Maastricht'), (10006, 'Clinique Du Mont-Luis', '', 'FR', 'Paris'), (10007, 'Centre Medico Chirurgical Paris V', '', 'FR', 'Paris')]
ambulances = [(20000, 'Ambulancezorg Nederland', 'AN'), (20001, 'UMCG Ambulancezorg', 'UMCGA'),]
call_centre = [(3000, 'Emergency Call Centre', 'ECC')]
print("Filling outcomes from FHIR standard AuditEvent => https://www.hl7.org/fhir/auditevent.html")
for entry in hospitals:
    Organisation.objects.create(
        organisationid=entry[0],
        organisationname=entry[1],
        organisationsector="",
        organisationcity=entry[4],
        organisationcountry=entry[3]
    )

##############################
##############################
country_cities = [("NO","Oslo"),("BY", "Minsk"), ("DE", "Berlin"), ("CZ", "Prague"), ("FR", "Paris"), ("GR", "Athens"), ("LU", "Luxembourg"), ("NL", "Amsterdam"), ("PT", "Lisbon"), ("SE", "Stockholm") ]

entity_what = ["Condition", "Allergy Intolerance", "Family Member History", "Medication", "Emergency Episode"]

print("Patients adding EMR or doing other events")
for identifier in tqdm(range(1,1001)):

    """
    try:
        response = DbIpCity.get(fake.ipv4_public(), api_key='free')
    except:
        response = DbIpCity.get('147.229.2.90', api_key='free')

    name = names.get_first_name()+" "+names.get_last_name()
    ent_what = random.choice(entity_what)
    timezone = fake.timezone()
    ip = fake.ipv4_private()
    continent = response.region
    country = response.country
    city = response.city[0:45]
    """

    name = names.get_first_name()+" "+names.get_last_name()
    ent_what = random.choice(entity_what)
    tmp_city = random.choice(country_cities)[1]
    timezone = "Europe/{}".format(tmp_city)
    ip = "0.0.0.0"
    continent = "Europe"
    country = random.choice(country_cities)[0]
    city = tmp_city


    for j in range(0, random.randint(1,3)):
        ApamLog.objects.create(
            logid=random.randint(1,1000000000),
            eventcode=Event.objects.get(eventcode=110110),
            actioncode=Action.objects.get(actioncode=10001),
            agentid=10000000+identifier,
            agentrole="Patient",
            agentname=name,
            agentattributes="",
            agentcontinent=continent,
            agentcountry=country,
            agentcity=city,
            agenttz=timezone,
            agentip=ip,
            organisationid=None,
            subjectid=(10000000+identifier),
            #subjectname=name,
            outcomecode=Outcome.objects.get(outcomecode=0),
            purposeofevent="",
            recorded=fake.date_between(start_date='-1y', end_date='today'),
            #identity=00000,
            entityobject="Record",
            entitydetails="Patient added {}".format(ent_what)
        )

    for j in range(0, random.randint(1,3)):
        ApamLog.objects.create(
            logid=random.randint(1,1000000000),
            eventcode=Event.objects.get(eventcode=random.choice([110106, 110103])),
            actioncode=Action.objects.get(actioncode=10005),
            agentid=(10000000+identifier),
            agentrole="Patient",
            agentname=name,
            agentattributes="",
            agentcontinent =continent,
            agentcountry=country,
            agentcity=city,
            agenttz=timezone,
            agentip=ip,
            organisationid=None,
            subjectid=(10000000+identifier),
            #subjectname=name,
            outcomecode=Outcome.objects.get(outcomecode=0),
            purposeofevent="",
            recorded=fake.date_between(start_date='-1y', end_date='today'),
            #identity=00000,
            entityobject="",
            entitydetails=""
        )

###################################################################################################
################### Healthcare professionals interaction with patient's data ######################
provinces = [("Drenthe", "Meppel"), ("Gelderland", "Ede"), ("Groningen","Adorp"), ("Flevoland", "Zeewolde"), ("Friesland", "Makkum"), ("Limburg","Maastricht"), ("Noord-Brabant", "Eindhoven"), ("Noord-Holland","Amsterdam"), ("Overijssel", "Hellendoorn"),
("Utrecht","Utrecht"), ("Zeeland", "Kijkuit"), ("Zuid-Holland", "Rotterdam")]
print("Doctors adding records to the patient")

ips = ["157.245.72.245", "62.148.163.211", "94.75.250.117"]

####################################################################################################
#################### Breakglass doctor reading, updating and adding DICOM FILE #####################
for identifier in tqdm(range(1,50)):
    doctor_id = 90000000+(random.randint(1,10))
    ip = random.choice(ips)
    name = names.get_first_name()+" "+names.get_last_name()
    ent_what = random.choice(entity_what)
    province = random.choice(provinces)[0]
    city = random.choice(provinces)[1]
    organisationid = random.choice(hospitals)[0]
    subject_id = 10000000+identifier
    date = fake.date_between(start_date='-1y', end_date='today')

    ApamLog.objects.create(
        logid=random.randint(1,1000000000),
        eventcode=Event.objects.get(eventcode=110110),
        actioncode=Action.objects.get(actioncode=10002),
        agentid=90000000+(random.randint(1,100)),
        agentrole="Neurologist",
        #agentdepartment="Neurology",
        agentname=name,
        agentattributes="role=Neurologist|dp=Neurology|breakglass=Allowed",
        agentcontinent ="Europe",
        agentcountry="NL",
        agentcity=city,
        agenttz="Europe/Amsterdam",
        agentip=ip,
        organisationid=Organisation.objects.get(organisationid=organisationid),
        subjectid=subject_id,
        #subjectname=name,
        outcomecode=Outcome.objects.get(outcomecode=0),
        purposeofevent="Breakglass Session",
        recorded=date,
        #identity=00000,
        entityobject="Record",
        #entitywhat="Condition",
        entitydetails="Doctor read {}".format(ent_what)
    )

    ApamLog.objects.create(
        logid=random.randint(1,1000000000),
        eventcode=Event.objects.get(eventcode=110110),
        actioncode=Action.objects.get(actioncode=10003),
        agentid=90000000+(random.randint(1,100)),
        agentrole="Radiologist",
        #agentdepartment="Radiology",
        agentname=name,
        agentattributes="role=Neurologist|dp=Neurology|breakglass=Allowed",
        agentcontinent ="Europe",
        agentcountry="NL",
        agentcity=city,
        agenttz="Europe/Amsterdam",
        agentip=ip,
        organisationid=Organisation.objects.get(organisationid=organisationid),
        subjectid=subject_id,
        #subjectname=name,
        outcomecode=Outcome.objects.get(outcomecode=0),
        purposeofevent="Breakglass Session",
        recorded=date,
        #identity=00000,
        entityobject="Record",
        #entitywhat="Condition",
        entitydetails="Doctor updated {}".format(ent_what)
    )

    ApamLog.objects.create(
        logid=random.randint(1,1000000000),
        eventcode=Event.objects.get(eventcode=110110),
        actioncode=Action.objects.get(actioncode=10001),
        agentid=90000000+(random.randint(1,100)),
        agentrole="Neurologist",
        #agentdepartment="Neurology",
        agentname=name,
        agentattributes="role=Neurologist|dp=Neurology|breakglass=Allowed",
        agentcontinent ="Europe",
        agentcountry="NL",
        agentcity=city,
        agenttz="Europe/Amsterdam",
        agentip=ip,
        organisationid=Organisation.objects.get(organisationid=organisationid),
        subjectid=subject_id,
        #subjectname=name,
        outcomecode=Outcome.objects.get(outcomecode=0),
        purposeofevent="Breakglass Session",
        recorded=date,
        #identity=00000,
        entityobject="Record",
        #entitywhat="Emergency Episode",
        entitydetails="Doctor added {}".format(ent_what)
    )
    
    ApamLog.objects.create(
        logid=random.randint(1,1000000000),
        eventcode=Event.objects.get(eventcode=110110),
        actioncode=Action.objects.get(actioncode=10002),
        agentid=90000000+(random.randint(1,100)),
        agentrole="Cardiologist",
        #agentdepartment="Cardiology",
        agentname=name,
        agentattributes="role=Neurologist|dp=Neurology|breakglass=Allowed",
        agentcontinent ="Europe",
        agentcountry="NL",
        agentcity=city,
        agenttz="Europe/Amsterdam",
        agentip=ip,
        organisationid=Organisation.objects.get(organisationid=organisationid),
        subjectid=subject_id,
        #subjectname=name,
        outcomecode=Outcome.objects.get(outcomecode=0),
        purposeofevent="Check Records During Consult",
        recorded=fake.date_between(start_date='-1y', end_date='today'),
        #identity=00000,
        entityobject="Record",
        #entitywhat="Medication",
        entitydetails="Doctor checked {}".format(ent_what)
    )

    ApamLog.objects.create(
        logid=random.randint(1,1000000000),
        eventcode=Event.objects.get(eventcode=110110),
        actioncode=Action.objects.get(actioncode=10002),
        agentid=90000000+(random.randint(1,100)),
        agentrole="Pharmacist",
        #agentdepartment="Pharmacy",
        agentname=name,
        agentattributes="role=Neurologist|dp=Neurology|breakglass=Allowed",
        agentcontinent ="Europe",
        agentcountry="NL",
        agentcity=city,
        agenttz="Europe/Amsterdam",
        agentip=ip,
        organisationid=Organisation.objects.get(organisationid=organisationid),
        subjectid=subject_id,
        #subjectname=name,
        outcomecode=Outcome.objects.get(outcomecode=0),
        purposeofevent="Check Medication Record",
        recorded=fake.date_between(start_date='-1y', end_date='today'),
        #identity=00000,
        entityobject="Record",
        #entitywhat="Medication",
        entitydetails="Doctor updated {}".format(ent_what)
    )

    chance_of_denied = 4
    chance_of_failure = 8
    if (random.randint(0,50) <= chance_of_denied):
        ApamLog.objects.create(
        logid=random.randint(1,1000000000),
        eventcode=Event.objects.get(eventcode=110110),
        actioncode=Action.objects.get(actioncode=10002),
        agentid=90000000+(random.randint(1,100)),
        agentrole="Nurse",
        #agentdepartment="Neurology",
        agentname=name,
        agentattributes="role=Neurologist|dp=Neurology|breakglass=Allowed",
        agentcontinent ="Europe",
        agentcountry="NL",
        agentcity=city,
        agenttz="Europe/Amsterdam",
        agentip=ip,
        organisationid=Organisation.objects.get(organisationid=organisationid),
        subjectid=subject_id,
        #subjectname=name,
        outcomecode=Outcome.objects.get(outcomecode=10),
        purposeofevent="Update Record During Consult",
        recorded=fake.date_between(start_date='-1y', end_date='today'),
        #identity=00000,
        entityobject="Record",
        #entitywhat="Medication",
        entitydetails="Doctor checked {}".format(ent_what)
    )

    if (random.randint(0,50) <= chance_of_failure):
        ApamLog.objects.create(
        logid=random.randint(1,1000000000),
        eventcode=Event.objects.get(eventcode=110110),
        actioncode=Action.objects.get(actioncode=10002),
        agentid=90000000+(random.randint(1,100)),
        agentrole="Pharmacist",
        #agentdepartment="Pharmacy",
        agentname=name,
        agentattributes="role=Neurologist|dp=Neurology|breakglass=Allowed",
        agentcontinent ="Europe",
        agentcountry="NL",
        agentcity=city,
        agenttz="Europe/Amsterdam",
        agentip=ip,
        organisationid=Organisation.objects.get(organisationid=organisationid),
        subjectid=subject_id,
        #subjectname=name,
        outcomecode=Outcome.objects.get(outcomecode=1),
        purposeofevent="Check medication record",
        recorded=fake.date_between(start_date='-1y', end_date='today'),
        #identity=00000,
        entityobject="Record",
        #entitywhat="Medication",
        entitydetails="Doctor checked {}".format(ent_what)
    )