# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class ApamLog(models.Model):
    logid = models.IntegerField(db_column='logID', primary_key=True)  # Field name made lowercase.
    eventcode = models.ForeignKey('Event', models.DO_NOTHING, db_column='eventCode', blank=True, null=True)  # Field name made lowercase.
    actioncode = models.ForeignKey('Action', models.DO_NOTHING, db_column='actionCode', blank=True, null=True)  # Field name made lowercase.
    recorded = models.DateTimeField(blank=True, null=True)
    period = models.IntegerField(blank=True, null=True)
    outcomecode = models.ForeignKey('Outcome', models.DO_NOTHING, db_column='outcomeCode', blank=True, null=True)  # Field name made lowercase.
    purposeofevent = models.CharField(db_column='purposeOfEvent', max_length=140, blank=True, null=True)  # Field name made lowercase.
    agentid = models.IntegerField(db_column='agentID', blank=True, null=True)  # Field name made lowercase.
    agentrole = models.CharField(db_column='agentRole', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agentname = models.CharField(db_column='agentName', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agentattributes = models.CharField(db_column='agentAttributes', max_length=140, blank=True, null=True)  # Field name made lowercase.
    agentpurposeofuse = models.CharField(db_column='agentPurposeOfUse', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agentcontinent = models.CharField(db_column='agentContinent', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agentcountry = models.CharField(db_column='agentCountry', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agentcity = models.CharField(db_column='agentCity', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agenttz = models.CharField(db_column='agentTZ', max_length=45, blank=True, null=True)  # Field name made lowercase.
    agentip = models.CharField(db_column='agentIP', max_length=45, blank=True, null=True)  # Field name made lowercase.
    sourceuseragent = models.CharField(db_column='sourceUserAgent', max_length=45, blank=True, null=True)  # Field name made lowercase.
    sourcetypecode = models.IntegerField(db_column='sourceTypeCode', blank=True, null=True)  # Field name made lowercase.
    organisationid = models.ForeignKey('Organisation', models.DO_NOTHING, db_column='organisationID', blank=True, null=True)  # Field name made lowercase.
    subjectid = models.IntegerField(db_column='subjectID', blank=True, null=True)  # Field name made lowercase.
    entityobject = models.CharField(db_column='entityObject', max_length=45, blank=True, null=True)  # Field name made lowercase.
    entitydetails = models.CharField(db_column='entityDetails', max_length=45, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'APAM Log'


class Action(models.Model):
    actioncode = models.IntegerField(db_column='actionCode', primary_key=True)  # Field name made lowercase.
    actionname = models.CharField(db_column='actionName', max_length=45, blank=True, null=True)  # Field name made lowercase.
    actiondescription = models.TextField(db_column='actionDescription', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Action'


class Event(models.Model):
    eventcode = models.IntegerField(db_column='eventCode', primary_key=True)  # Field name made lowercase.
    eventname = models.CharField(db_column='eventName', max_length=45, blank=True, null=True)  # Field name made lowercase.
    eventdescription = models.CharField(db_column='eventDescription', max_length=280, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Event'


class Organisation(models.Model):
    organisationid = models.IntegerField(db_column='organisationID', primary_key=True)  # Field name made lowercase.
    organisationname = models.CharField(db_column='organisationName', max_length=90, blank=True, null=True)  # Field name made lowercase.
    organisationsector = models.CharField(db_column='organisationSector', max_length=90, blank=True, null=True)  # Field name made lowercase.
    organisationcountry = models.CharField(db_column='organisationCountry', max_length=45, blank=True, null=True)  # Field name made lowercase.
    organisationcity = models.CharField(db_column='organisationCity', max_length=45, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Organisation'


class Outcome(models.Model):
    outcomecode = models.IntegerField(db_column='outcomeCode', primary_key=True)  # Field name made lowercase.
    outcomename = models.CharField(db_column='outcomeName', max_length=45, blank=True, null=True)  # Field name made lowercase.
    outcomedescription = models.CharField(db_column='outcomeDescription', max_length=280, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Outcome'
