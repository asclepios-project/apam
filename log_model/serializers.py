from rest_framework import serializers
from .models import ApamLog

class ApamLogSerializer(serializers.ModelSerializer):

    class Meta:

        model = ApamLog
        fields = '__all__'